package com.yorosoft.reactiveeexpenseapi.controller;

import static com.yorosoft.reactiveeexpenseapi.config.CategoryControllerAPIPaths.BASE_PATH;
import static com.yorosoft.reactiveeexpenseapi.config.CategoryControllerAPIPaths.CREATE;
import static com.yorosoft.reactiveeexpenseapi.config.CategoryControllerAPIPaths.GET_ALL;

import com.yorosoft.reactiveeexpenseapi.ReactiveEExpenseApiApplication;
import com.yorosoft.reactiveeexpenseapi.model.Purchase;
import com.yorosoft.reactiveeexpenseapi.util.PurchaseGenerator;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

@Slf4j
@SpringBootTest(
    classes = ReactiveEExpenseApiApplication.class,
    webEnvironment = WebEnvironment.RANDOM_PORT
)
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PurchaseControllerTest {

  private static WebTestClient webTestClient;
  private static Purchase purchase = PurchaseGenerator.generatePurchase();

  @LocalServerPort
  int port;

  @Autowired
  public void setApplicationContext(ApplicationContext context) {
    webTestClient = WebTestClient
        .bindToApplicationContext(context)
        .configureClient()
        .baseUrl(BASE_PATH)
        .build();
  }

  @Test
  @Order(10)
  void itShouldGetAllPurchases() {
    webTestClient
        .get()
        .uri(GET_ALL)
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus().isOk()
        .expectHeader().contentType(MediaType.APPLICATION_JSON)
        .expectBody()
        .jsonPath("$.[0].id").isNotEmpty()
        .jsonPath("$.[0].name").isNotEmpty();
  }

  @Test
  @Order(20)
  void itShouldCreatePurchase() {
    Purchase purchase = PurchaseGenerator.generatePurchase();
    purchase.setId(null);

    webTestClient
        .post()
        .uri(CREATE)
        .contentType(MediaType.APPLICATION_JSON)
        .body(Mono.just(purchase), Purchase.class)
        .exchange()
        .expectStatus().isCreated()
        .expectHeader().contentType(MediaType.APPLICATION_JSON);
  }
}
