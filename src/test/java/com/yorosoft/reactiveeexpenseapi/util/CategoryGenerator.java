package com.yorosoft.reactiveeexpenseapi.util;

import com.yorosoft.reactiveeexpenseapi.model.Category;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

public class CategoryGenerator {

  private static final Instant now = Instant.now();

  public static Category generateCategory() {
    return generateCategory(1000L);
  }

  /**
   * Generate sample Category Item which will be used in test classes
   *
   * @return category
   */
  private static Category generateCategory(Long id) {
    Category category = new Category();
    category.setId(id);
    category.setName("Equipement");
    category.setCreatedOn(now);
    return category;
  }

  public static List<Category> generateCategories() {
    return LongStream.range(1, 100)
        .mapToObj(CategoryGenerator::generateCategory).collect(Collectors.toList());
  }
}
