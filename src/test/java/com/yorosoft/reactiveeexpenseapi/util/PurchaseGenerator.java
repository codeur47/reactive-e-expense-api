package com.yorosoft.reactiveeexpenseapi.util;

import com.yorosoft.reactiveeexpenseapi.model.Purchase;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

public class PurchaseGenerator {

  private static final Instant now = Instant.now();

  public static Purchase generatePurchase() {
    return generatePurchase(1000L);
  }

  /**
   * Generate sample Purchase Item which will be used in test classes
   *
   * @return Purchase
   */
  private static Purchase generatePurchase(Long id) {
    Purchase purchase = new Purchase();
    purchase.setName("Velo");
    purchase.setDescription("Velo de la velontiere");
    purchase.setQuantity(3);
    purchase.setPrice(23);
    purchase.setTotal(456);
    purchase.setCreatedOn(now);
    purchase.setCategoryId(5L);
    return purchase;
  }

  public static List<Purchase> generaPurchaseList() {
    return LongStream.range(1, 100)
        .mapToObj(PurchaseGenerator::generatePurchase).collect(Collectors.toList());
  }
}
