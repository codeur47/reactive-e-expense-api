package com.yorosoft.reactiveeexpenseapi.config;

import java.util.Collections;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebFlux;

@Configuration
@EnableSwagger2WebFlux
public class SpringFoxConfig {

  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.basePackage("com.yorosoft.reactiveeexpenseapi"))
        .paths(PathSelectors.any())
        .build()
        .apiInfo(apiInfo());
  }

  private ApiInfo apiInfo() {
    return new ApiInfo(
        "Reactive E-expense API",
        "Spring Reactive Restful APIs for Home Expenses Management System.",
        "0.0.1-SNAPSHOT",
        "Terms of service",
        new Contact("Ange Carmel, YORO", "https://fr.worldline.com/", "yoropapers@gmail.com"),
        "License of API", "API license URL", Collections.emptyList());
  }
}
