package com.yorosoft.reactiveeexpenseapi.config;

/**
 * Class to hold Purchase API Paths used across the controller classes
 *
 * @author Ange Carmel YORO
 */
public class PurchaseControllerAPIPaths {


  public static final String BASE_PATH = "/api/v1/purchase";

  public static final String CREATE = "/";
  public static final String GET_ALL = "/";
  public static final String GET_BY_NAME = "/{name}";
  public static final String GET_BY_ID = "/{id}";
  public static final String GET_ALL_BY_CATEGORY_ID = "/category/{id}";
  public static final String UPDATE = "/";
  public static final String DELETE = "/{id}";

}
