package com.yorosoft.reactiveeexpenseapi.config;

/**
 * Class to hold Category API Paths used across the controller classes
 *
 * @author Ange Carmel YORO
 */
public class CategoryControllerAPIPaths {

  public static final String BASE_PATH = "/api/v1/category";
  public static final String CREATE = "/";
  public static final String GET_ALL = "/";
  public static final String GET_BY_NAME = "/cat/{name}";
  public static final String GET_BY_ID = "/{id}";
  public static final String UPDATE = "/{id}";
  public static final String DELETE = "/{id}";

}
