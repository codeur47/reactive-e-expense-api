package com.yorosoft.reactiveeexpenseapi.controller;

import com.yorosoft.reactiveeexpenseapi.config.PurchaseControllerAPIPaths;
import com.yorosoft.reactiveeexpenseapi.exception.ResourceNotFoundException;
import com.yorosoft.reactiveeexpenseapi.model.Purchase;
import com.yorosoft.reactiveeexpenseapi.model.ResourceIdentity;
import com.yorosoft.reactiveeexpenseapi.service.PurchaseService;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Controller class to handle Purchase Item CRUD operations
 *
 * @author Ange Carmel YORO
 */

@Slf4j
@RestController
@RequestMapping(PurchaseControllerAPIPaths.BASE_PATH)
public class PurchaseController {

  private final PurchaseService purchaseService;

  @Autowired
  public PurchaseController(PurchaseService purchaseService) {
    this.purchaseService = purchaseService;
  }

  /**
   * Get Purchases Items available in database
   *
   * @return purchases
   */
  @GetMapping(PurchaseControllerAPIPaths.GET_ALL)
  @ResponseStatus(value = HttpStatus.OK)
  public Flux<Purchase> getPurchases() { return purchaseService.getPurchases();}

  /**
   * Get Purchases Items available in database by Category Id
   *
   * @return purchases
   */
  @GetMapping(PurchaseControllerAPIPaths.GET_ALL_BY_CATEGORY_ID)
  @ResponseStatus(value = HttpStatus.OK)
  public Flux<Purchase> getPurchasesByCategoryId(@PathVariable(value = "id") Long id) { return purchaseService.getPurchasesByCategoryId(id);}


  /**
   * Get Purchase Item by ID
   *
   * @param id
   * @return purchase Item
   * @throws ResourceNotFoundException
   */
  @GetMapping(PurchaseControllerAPIPaths.GET_BY_ID)
  public Mono<Purchase> getPurchaseById(@PathVariable(value = "id") Long id) throws ResourceNotFoundException {
    return purchaseService.getPurchaseById(id);
  }

  /**
   * Get Purchase Item by Name
   *
   * @param name
   * @return purchase Item
   * @throws ResourceNotFoundException
   */
  @GetMapping(PurchaseControllerAPIPaths.GET_BY_NAME)
  public Mono<Purchase> getPurchaseById(@PathVariable(value = "name") String  name) throws ResourceNotFoundException {
    return purchaseService.getPurchaseByName(name);
  }

  /**
   * Create Purchase Item
   *
   * @param purchase
   * @return id of created Purchase Item
   */
  @PostMapping(PurchaseControllerAPIPaths.CREATE)
  @ResponseStatus(value = HttpStatus.CREATED)
  public Mono<ResponseEntity> addPurchase(@Valid @RequestBody Purchase purchase) {

    Mono<Long> id = purchaseService.addPurchase(purchase);

    return id
        .map(value -> ResponseEntity.status(HttpStatus.CREATED).body(new ResourceIdentity(value)))
        .cast(ResponseEntity.class);
  }

  /**
   * Update Purchase Item by ID
   *
   * @param purchase
   * @throws ResourceNotFoundException
   */
  @PutMapping(PurchaseControllerAPIPaths.UPDATE)
  @ResponseStatus(value = HttpStatus.OK)
  public void updatePurchase(@Valid @RequestBody Purchase purchase) throws ResourceNotFoundException {
    purchaseService.updatePurchase(purchase);
  }

  /**
   * Delete Purchase Item by ID
   *
   * @param id
   * @throws ResourceNotFoundException
   */
  @DeleteMapping(PurchaseControllerAPIPaths.DELETE)
  @ResponseStatus(value = HttpStatus.NO_CONTENT)
  public void removePurchase(@PathVariable(value = "id") Long id) throws ResourceNotFoundException {
    Mono<Purchase> purchaseMonoFromDB = purchaseService.getPurchaseById(id);
    purchaseMonoFromDB.subscribe(
        purchaseService::deletePurchase
    );
  }
}
