package com.yorosoft.reactiveeexpenseapi.controller;

import com.yorosoft.reactiveeexpenseapi.config.CategoryControllerAPIPaths;
import com.yorosoft.reactiveeexpenseapi.exception.ResourceNotFoundException;
import com.yorosoft.reactiveeexpenseapi.model.Category;
import com.yorosoft.reactiveeexpenseapi.model.ResourceIdentity;
import com.yorosoft.reactiveeexpenseapi.service.CategoryService;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Controller class to handle Category Item CRUD operations
 *
 * @author Ange Carmel YORO
 */

@Slf4j
@RestController
@RequestMapping(CategoryControllerAPIPaths.BASE_PATH)
public class CategoryController {

  private final CategoryService categoryService;

  @Autowired
  public CategoryController(CategoryService categoryService) {
    this.categoryService = categoryService;
  }

  /**
   * Get Categories Items available in database
   *
   * @return categories
   */
  @GetMapping(value = CategoryControllerAPIPaths.GET_ALL, produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(value = HttpStatus.OK)
  public Flux<Category> getCategories() {
    return categoryService.getCategories();
  }

  /**
   * Get Category Item by ID
   *
   * @param id
   * @return category Item
   * @throws ResourceNotFoundException
   */
  @GetMapping(value = CategoryControllerAPIPaths.GET_BY_ID, produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<Category> getCategoryById(@PathVariable(value = "id") Long id)
      throws ResourceNotFoundException {
    return categoryService.getCategoryById(id);
  }

  /**
   * Get Category Item by Name
   *
   * @param name
   * @return category Item
   * @throws ResourceNotFoundException
   */
  @GetMapping(value = CategoryControllerAPIPaths.GET_BY_NAME, produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<Category> getCategoriesByName(@PathVariable(value = "name") String name)
      throws ResourceNotFoundException {
    return categoryService.getCategoryByName(name);
  }

  /**
   * Create Category Item
   *
   * @param category
   * @return id of created Category Item
   */
  @PostMapping(value = CategoryControllerAPIPaths.CREATE, consumes = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(value = HttpStatus.CREATED)
  public Mono<ResponseEntity> addCategory(@Valid @RequestBody Category category){

    Mono<Long> id = categoryService.addCategory(category);

    return id
        .map(value -> ResponseEntity.status(HttpStatus.CREATED).body(new ResourceIdentity(value)))
        .cast(ResponseEntity.class);
  }

  /**
   * Update Category Item by ID
   *
   * @param id
   * @param category
   * @throws ResourceNotFoundException
   */
  @PutMapping(value = CategoryControllerAPIPaths.UPDATE, consumes = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(value = HttpStatus.OK)
  public void updateCategory(@PathVariable(value = "id") Long id, @Valid @RequestBody Category category) throws ResourceNotFoundException {
    categoryService.updateCategory(category);
  }


  /**
   * Delete Category Item by ID
   *
   * @param id
   * @throws ResourceNotFoundException
   */
  @DeleteMapping(CategoryControllerAPIPaths.DELETE)
  @ResponseStatus(value = HttpStatus.NO_CONTENT)
  public void removeCategory(@PathVariable(value = "id") Long id) throws ResourceNotFoundException {

    Mono<Category> category = categoryService.getCategoryById(id);
    category.subscribe(
        categoryService::deleteCategory
    );
  }

}
