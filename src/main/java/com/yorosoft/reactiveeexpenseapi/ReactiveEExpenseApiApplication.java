package com.yorosoft.reactiveeexpenseapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.reactive.config.EnableWebFlux;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableWebFlux
public class ReactiveEExpenseApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReactiveEExpenseApiApplication.class, args);
	}

}
