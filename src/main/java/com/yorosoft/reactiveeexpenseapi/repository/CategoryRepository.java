package com.yorosoft.reactiveeexpenseapi.repository;

import com.yorosoft.reactiveeexpenseapi.model.Category;
import org.springframework.data.repository.reactive.ReactiveSortingRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface CategoryRepository extends ReactiveSortingRepository<Category, Long> {
  Mono<Category> findByName(String name);
}
