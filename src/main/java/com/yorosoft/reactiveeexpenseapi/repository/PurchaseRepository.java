package com.yorosoft.reactiveeexpenseapi.repository;

import com.yorosoft.reactiveeexpenseapi.model.Purchase;
import org.springframework.data.repository.reactive.ReactiveSortingRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface PurchaseRepository extends ReactiveSortingRepository<Purchase, Long> {
  Flux<Purchase> findAllByCategoryId(Long id);
  Mono<Purchase> findByName(String name);
}
