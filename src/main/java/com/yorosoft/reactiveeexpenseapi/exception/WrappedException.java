package com.yorosoft.reactiveeexpenseapi.exception;

public class WrappedException extends RuntimeException {

  public WrappedException(Throwable e) {
    super(e);
  }
}
