package com.yorosoft.reactiveeexpenseapi.service;

import com.yorosoft.reactiveeexpenseapi.exception.ResourceNotFoundException;
import com.yorosoft.reactiveeexpenseapi.model.Category;
import com.yorosoft.reactiveeexpenseapi.repository.CategoryRepository;
import java.time.Instant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
/**
 * Service class to handle Category Item CRUD Operations.
 *
 * @author Ange Carmel YORO
 */
@Slf4j
@Service
public class CategoryService {

  private final CategoryRepository categoryRepository;

  @Autowired
  public CategoryService(
      CategoryRepository categoryRepository) {
    this.categoryRepository = categoryRepository;
  }

  public Flux<Category> getCategories() {
    Sort sort = Sort.by(Direction.ASC,"name");
    return categoryRepository.findAll(sort);
  }

  public Mono<Category> getCategoryById(Long id) throws ResourceNotFoundException {
    return _getCategoryById(id);
  }

  public Mono<Category> getCategoryByName(String name) throws ResourceNotFoundException {
    return _getCategoryByName(name);
  }

  private Mono<Category> _getCategoryById(Long id) throws ResourceNotFoundException {
    return categoryRepository.findById(id)
        .switchIfEmpty(Mono.defer(() -> Mono.error(new ResourceNotFoundException(String.format("Category not found for the provided ID :: %s%", id)))));
  }

  private Mono<Category> _getCategoryByName(String name) throws ResourceNotFoundException {
    return categoryRepository.findByName(name)
        .switchIfEmpty(Mono.defer(() -> Mono.error(new ResourceNotFoundException(String.format("Category not found for the provided name :: %s%", name)))));
  }

  public Mono<Long> addCategory(Category category) {
    category.setCreatedOn(Instant.now());

    return categoryRepository
        .save(category)
        .flatMap(item -> Mono.just(item.getId()));
  }

  public void updateCategory(Category category) throws ResourceNotFoundException {
    Mono<Category> categoryMonoFromDB = getCategoryById(category.getId());

    categoryMonoFromDB.subscribe(
        value -> {
          value.setName(category.getName());
          value.setUpdatedOn(Instant.now());

          categoryRepository
              .save(value)
              .subscribe();
        });
  }

  public void deleteCategory(Category category) {
    categoryRepository.delete(category).subscribe();
  }

}
