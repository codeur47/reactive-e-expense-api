package com.yorosoft.reactiveeexpenseapi.service;

import com.yorosoft.reactiveeexpenseapi.exception.ResourceNotFoundException;
import com.yorosoft.reactiveeexpenseapi.model.Purchase;
import com.yorosoft.reactiveeexpenseapi.repository.PurchaseRepository;
import java.time.Instant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service class to handle Purchase Item CRUD Operations.
 *
 * @author Ange Carmel YORO
 */
@Slf4j
@Service
public class PurchaseService {

  private final PurchaseRepository purchaseRepository;

  @Autowired
  public PurchaseService(PurchaseRepository purchaseRepository) {
    this.purchaseRepository = purchaseRepository;
  }

  public Flux<Purchase> getPurchases() {
    Sort sort = Sort.by(Direction.ASC,"name");
    return purchaseRepository.findAll(sort);
  }

  public Mono<Purchase> getPurchaseById(Long id) throws ResourceNotFoundException {
    return _getPurchaseById(id);
  }

  public Mono<Purchase> getPurchaseByName(String name) throws ResourceNotFoundException {
    return _getPurchaseByName(name);
  }

  public Mono<Long> addPurchase(Purchase purchase) {
    purchase.setCreatedOn(Instant.now());

    return
        purchaseRepository
        .save(purchase)
        .flatMap(item -> Mono.just(item.getId()));
  }

  public void updatePurchase(Purchase purchase) throws ResourceNotFoundException {
    Mono<Purchase> purchaseMonoFromDB = getPurchaseById(purchase.getId());

    purchaseMonoFromDB.subscribe(
        value -> {
          value.setName(purchase.getName());
          value.setDescription(purchase.getDescription());
          value.setPrice(purchase.getPrice());
          value.setQuantity(purchase.getQuantity());
          value.setTotal(purchase.getTotal());
          value.setUpdatedOn(Instant.now());

          purchaseRepository
              .save(value)
              .subscribe();
        });
  }

  public void deletePurchase(Purchase purchase) {
    purchaseRepository.delete(purchase).subscribe();
  }

  public Flux<Purchase> getPurchasesByCategoryId(Long id) {
    return purchaseRepository.findAllByCategoryId(id);
  }


  private Mono<Purchase> _getPurchaseById(Long id) throws ResourceNotFoundException{
    return purchaseRepository.findById(id)
        .switchIfEmpty(Mono.defer(() -> Mono.error(new ResourceNotFoundException(String.format("Purchase not found for the provided ID :: %s%", id)))));
  }

  private Mono<Purchase> _getPurchaseByName(String name) throws ResourceNotFoundException{
    return purchaseRepository.findByName(name)
        .switchIfEmpty(Mono.defer(() -> Mono.error(new ResourceNotFoundException(String.format("Purchase not found for the provided Name :: %s%", name)))));
  }
}
