package com.yorosoft.reactiveeexpenseapi.model;

import java.time.Instant;
import javax.validation.constraints.NotEmpty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Data
@NoArgsConstructor
@Table("users")
public class User {

  @Id
  @Column("id")
  private Long id;

  @NotEmpty(message = "Lastname cannot be null or empty")
  @NonNull
  @Column("lastname")
  private String lastname;

  @NotEmpty(message = "Firstname cannot be null or empty")
  @NonNull
  @Column("firstname")
  private String firstname;

  @NotEmpty(message = "Username cannot be null or empty")
  @NonNull
  @Column("username")
  private String username;

  @NotEmpty(message = "Password cannot be null or empty")
  @NonNull
  @Column("password")
  private String password;

  @NonNull
  @Column("created_on")
  private Instant createdOn;

  @Column("updated_on")
  private Instant updatedOn;

}
