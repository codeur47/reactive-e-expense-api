package com.yorosoft.reactiveeexpenseapi.model;

import java.time.Instant;
import javax.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Data
@NoArgsConstructor
@Table("purchase")
public class Purchase {

  @Id
  @Column("id")
  private Long id;

  @NotEmpty(message = "name cannot be null or empty")
  @NonNull
  @Column("name")
  private String name;

  @NotEmpty(message = "description cannot be null or empty")
  @NonNull
  @Column("description")
  private String description;

  @Column("category_id")
  private Long categoryId;

  @Column("price")
  private Integer price;


  @Column("quantity")
  private Integer quantity;


  @Column("total")
  private Integer total;

  @NonNull
  @Column("created_on")
  private Instant createdOn;

  @Column("updated_on")
  private Instant updatedOn;

}
