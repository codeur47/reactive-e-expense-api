package com.yorosoft.reactiveeexpenseapi.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum ERole {

  USER("User"),
  ADMIN("Admin");

  @Getter
  private final String value;

}
