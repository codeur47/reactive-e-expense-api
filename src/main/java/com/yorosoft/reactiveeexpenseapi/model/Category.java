package com.yorosoft.reactiveeexpenseapi.model;

import java.time.Instant;
import javax.validation.constraints.NotEmpty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Data
@NoArgsConstructor
@Table("category")
public class Category {

  @Id
  @Column("id")
  private Long id;

  @NotEmpty(message = "name cannot be null or empty")
  @NonNull
  @Column("name")
  private String name;

  @NonNull
  @Column("created_on")
  private Instant createdOn;

  @Column("updated_on")
  private Instant updatedOn;

}
